use aoc_runner_derive::{aoc, aoc_generator};

#[aoc_generator(day1)]
fn get_input(input: &str) -> Vec<u64> {
    input.lines().map(|x| x.parse::<u64>().unwrap()).collect()
}

#[aoc(day1, part1)]
fn part1(inputs: &[u64]) -> u64 {
    inputs
        .windows(2)
        .fold(0, |acc, pair| if pair[1] > pair[0] { acc + 1 } else { acc })
}

#[aoc(day1, part2)]
fn part2(inputs: &[u64]) -> u64 {
    inputs.windows(4).fold(0, |acc, pair| {
        if pair[1..4].iter().sum::<u64>() > pair[0..3].iter().sum::<u64>() {
            acc + 1
        } else {
            acc
        }
    })
}
