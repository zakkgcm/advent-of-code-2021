use aoc_runner_derive::{aoc, aoc_generator};
use ndarray::{arr2, stack, Array2, Axis};

#[aoc_generator(day3)]
fn get_input(input: &str) -> Array2<u32> {
    let ncols = input.lines().next().unwrap().len();
    let input_data: Vec<u32> = input
        .lines()
        .map(|line| {
            line.chars()
                .map(|x| x.to_digit(2).unwrap())
                .collect::<Vec<_>>()
        })
        .flatten()
        .collect();

    Array2::from_shape_vec((input_data.len() / ncols, ncols), input_data).unwrap()
}

#[aoc(day3, part1)]
fn part1(inputs: &Array2<u32>) -> u64 {
    let gamma = inputs
        .sum_axis(Axis(0))
        .mapv(|a| (a > ((inputs.shape()[0] as u32) / 2)))
        .fold(0, |acc, &bit| (acc << 1) | (bit as u64));
    let epsilon = inputs
        .sum_axis(Axis(0))
        .mapv(|a| (a < ((inputs.shape()[0] as u32) / 2)))
        .fold(0, |acc, &bit| (acc << 1) | (bit as u64));

    gamma * epsilon
}

#[aoc(day3, part2)]
fn part2(inputs: &Array2<u32>) -> u64 {
    // so rust ndarray doesnt have masking BOOO

    let mut current_array = inputs.clone();
    let mut cur_col = 0;

    while current_array.shape()[0] > 1 {
        let the_bit = current_array.index_axis(Axis(1), cur_col).sum() as f32
            >= ((current_array.shape()[0] as f32) / 2.0);

        current_array = stack(
            Axis(0),
            current_array
                .axis_iter(Axis(0))
                .filter(|val| val[cur_col] == the_bit as u32)
                .collect::<Vec<_>>()
                .as_ref(),
        )
        .unwrap();

        cur_col += 1;
    }

    let oxy = current_array.fold(0, |acc, &bit| (acc << 1) | (bit as u64));

    current_array = inputs.clone();
    cur_col = 0;

    while current_array.shape()[0] > 1 {
        let the_bit = (current_array.index_axis(Axis(1), cur_col).sum() as f32)
            < ((current_array.shape()[0] as f32) / 2.0);

        current_array = stack(
            Axis(0),
            current_array
                .axis_iter(Axis(0))
                .filter(|val| val[cur_col] == the_bit as u32)
                .collect::<Vec<_>>()
                .as_ref(),
        )
        .unwrap();

        cur_col += 1;
    }

    let co2 = current_array.fold(0, |acc, &bit| (acc << 1) | (bit as u64));

    oxy * co2
}
