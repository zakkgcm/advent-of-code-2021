use aoc_runner_derive::{aoc, aoc_generator};
use itertools::Itertools;

struct Command {
    horizontal: i64,
    depth: i64,
}

#[aoc_generator(day2)]
fn get_input(input: &str) -> Vec<Command> {
    input
        .lines()
        .map(|line| {
            let (cmd_type, input) = line.splitn(2, ' ').collect_tuple().unwrap();
            let value = input.parse::<i64>().unwrap();

            match cmd_type {
                "forward" => Command {
                    horizontal: value,
                    depth: 0,
                },
                "down" => Command {
                    horizontal: 0,
                    depth: value,
                },
                "up" => Command {
                    horizontal: 0,
                    depth: -value,
                },
                _ => panic!("the submarine crashed, everyone died."),
            }
        })
        .collect()
}

#[aoc(day2, part1)]
fn part1(inputs: &[Command]) -> i64 {
    let (total_horiz, total_depth) = inputs.iter().fold((0, 0), |(horizontal, depth), command| {
        (horizontal + command.horizontal, depth + command.depth)
    });

    total_horiz * total_depth
}

#[aoc(day2, part2)]
fn part2(inputs: &[Command]) -> i64 {
    let (horiz, depth, aim) = inputs
        .iter()
        .fold((0, 0, 0), |(horizontal, depth, aim), command| {
            (
                horizontal + command.horizontal,
                depth + command.horizontal * aim,
                aim + command.depth,
            )
        });

    horiz * depth
}
