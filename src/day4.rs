use aoc_runner_derive::{aoc, aoc_generator};
use std::collections::{HashMap, VecDeque};

const GRID_SIZE: usize = 5;

// Store the Board in 3 parts:
// A 1D array with stride of GRID_SIZE, row major
// s.t. (i, j) -> i * GRID_SIZE + j
// and i = index / GRID_SIZE, j = index % GRID_SIZE
// A mapping from number -> index for easy lookup
// A mapping from number -> marked for easy win+score computation
#[derive(Debug, Clone)]
struct Board {
    is_winner: bool,

    grid: [u32; GRID_SIZE * GRID_SIZE],
    numbers: HashMap<u32, usize>,
    marked: HashMap<u32, bool>,
}

impl Board {
    fn mark(&mut self, number: u32) {
        // it's bingo! not every board has every number
        if !self.numbers.contains_key(&number) {
            return;
        }

        self.marked.insert(number, true);

        // check if we now have a bingo in this marked number's row or column
        let index = *self.numbers.get(&number).unwrap();
        let row = (index / GRID_SIZE) * GRID_SIZE;
        let col = index % GRID_SIZE;

        // check rows by just checking the set of neighboring elements
        // check cols by skipping every nth element
        // cargo fmt HATES this
        if self.grid[row..(row + GRID_SIZE)]
            .iter()
            .all(|x| *self.marked.get(x).unwrap_or(&false))
            || self
                .grid
                .iter()
                .skip(col)
                .step_by(GRID_SIZE)
                .all(|x| *self.marked.get(x).unwrap_or(&false))
        {
            self.is_winner = true;
        }
    }

    fn calc_score(&self) -> u32 {
        self.grid
            .iter()
            .filter(|x| !self.marked.contains_key(&x))
            .sum()
    }

    fn from_values(values: &[u32]) -> Board {
        // We need a fixed-size array
        // is there some more sane way to do this? this feels wrong
        let mut grid = [0; GRID_SIZE * GRID_SIZE];
        for i in 0..(GRID_SIZE * GRID_SIZE) {
            grid[i] = values[i];
        }

        Board {
            is_winner: false,
            grid: grid,
            numbers: values
                .iter()
                .enumerate()
                .map(|(i, x)| (*x, i))
                .collect::<HashMap<_, _>>(),
            marked: HashMap::new(),
        }
    }
}

#[aoc_generator(day4)]
fn get_input(input: &str) -> (Vec<u32>, Vec<Vec<u32>>) {
    let mut lines = input.lines();
    let drawings = lines
        .next()
        .unwrap()
        .split(',')
        .map(|x| x.parse::<u32>().unwrap())
        .collect::<Vec<_>>();

    let values = lines
        .collect::<Vec<_>>()
        .chunks(GRID_SIZE + 1)
        .map(|chunk| {
            chunk
                .iter()
                .skip(1)
                .map(|line| {
                    line.split_whitespace()
                        .map(|x| x.parse::<u32>().unwrap())
                        .collect::<Vec<_>>()
                })
                .flatten()
                .collect::<Vec<_>>()
        })
        .collect::<Vec<_>>();

    (drawings, values)
}

#[aoc(day4, part1)]
fn part1(inputs: &(Vec<u32>, Vec<Vec<u32>>)) -> u64 {
    let (drawings, values) = inputs;

    let mut boards = values
        .iter()
        .map(|input| Board::from_values(input))
        .collect::<Vec<_>>();

    for drawing in drawings {
        for board in &mut boards {
            board.mark(*drawing);
            if board.is_winner {
                return (board.calc_score() * drawing) as u64;
            }
        }
    }

    0
}

#[aoc(day4, part2)]
fn part2(inputs: &(Vec<u32>, Vec<Vec<u32>>)) -> u64 {
    let (drawings, values) = inputs;

    let mut boards = values
        .iter()
        .map(|input| Board::from_values(input))
        .collect::<VecDeque<_>>();

    for drawing in drawings {
        // use boards like a dynamic ring queue
        // push the board back in if it didnt win
        // or break out if it was the last one and won
        let num_boards = boards.len();
        for _ in 0..num_boards {
            let mut board = boards.pop_front().unwrap();

            board.mark(*drawing);

            if board.is_winner {
                if num_boards == 1 {
                    return (board.calc_score() * drawing) as u64;
                }
            } else {
                boards.push_back(board);
            }
        }
    }

    0
}
